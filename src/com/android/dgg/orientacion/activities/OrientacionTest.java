/**
 *
 */
package com.android.dgg.orientacion.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.dgg.orientacion.utils.OrientacionListener;
import com.android.dgg.orientacion.utils.OrientacionManager;
import com.android.dgg.orientacion.utils.Utils;

/**
 * @author david
 *
 */
public class OrientacionTest extends Activity {
    /** TAG para el Logger*/
    private static final String TAG = "activities.OrientacionTest";
    /** Botón para configurar nuestras preferencias */
    private Button mConfigure = null;
    /** Manager de operaciones del sensor orientación */
    private OrientacionManager mSensorOrientacion = null;
    /** Preferencias  */
    private SharedPreferences mSharedPrefs = null;
    /**
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        mConfigure = (Button) findViewById(R.id.btnConfig);
        mConfigure.setOnClickListener(configureClickListener);
        mSensorOrientacion = new OrientacionManager(this, mOrientacionListener);
        startListening();
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
    }
    /**
     * Método que comienza a "escuchar" cambios en el sensor orientacion si este está soportado
     * por el dispositivo
     */
    private void startListening() {
        if (mSensorOrientacion.isOrientationSensorSupported()) {
            Log.d(TAG, "Ready to start listening Orientation Sensor");
            mSensorOrientacion.startListening();
        } else {
            Log.d(TAG, "Orientation Sensor is not supported");
        }
    }

    /**
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        startListening();
    }
    /**
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
        stopListening();
        super.onPause();
    }
    /**
     * Método que deja de escuchar cambios en el sensor de orientación si este está soportado por el
     * dispositivo
     */
    private void stopListening() {
        if (mSensorOrientacion.isOrientationSensorSupported() && mSensorOrientacion.isRunning()) {
            Log.d(TAG, "Stop listening accelerometer");
            mSensorOrientacion.stopListening();
        } else {
            Log.d(TAG, "Accelerometer not supported / not running");
        }
    }

    /** Listener de operaciones a ejecutar cuando salten eventos con el sensor orientación */
    OrientacionListener mOrientacionListener = new OrientacionListener() {
        /** Check de settings wifi*/
        private Boolean mIsCheckedWifi;
        /** Check de settings de la vibración del teléfono*/
        private Boolean mIsCheckedVibrate;
        /** Seleccionar tono del teléfono
         * (En el onScreenUp seleccionaremos el Default Ringtone siempre)
         */
        private String mRingtone;
        private Utils mUtils = null;
        /**
         *  @see com.android.dgg.orientacion.utils.OrientacionListener#onScreenUp()
         */
        @Override
        public void onScreenUp() {
            Log.d(TAG, "OnScreenUp");
            mUtils = (mUtils == null) ? new Utils(OrientacionTest.this) : mUtils;
            //Habilitamos todo independiente de su estado en las preferencias
            mUtils.habilitarVibracion();
            mUtils.habilitarWifi();
            mUtils.establecerTonoPorDefecto();
        }

        private void getConfigurationSettings() {
             mSharedPrefs = (SharedPreferences)
                PreferenceManager.getDefaultSharedPreferences(OrientacionTest.this);
            mIsCheckedWifi = mSharedPrefs.getBoolean("wificheck", false);
            mIsCheckedVibrate = mSharedPrefs.getBoolean("vibrate", false);
            mRingtone = mSharedPrefs.getString("ringtone", "DEFAULT_RINGTONE_URI");
            mUtils = new Utils(OrientacionTest.this);
        }
        /**
         *  @see com.android.dgg.orientacion.utils.OrientacionListener#onScreenDown()
         */
        @Override
        public void onScreenDown() {
            Log.d(TAG, "OnScreenDown");
            getConfigurationSettings();
            //Ejecutamos la deshabilitación indicada en las preferencias
            if (mIsCheckedWifi) {
                final boolean disabled = mUtils.deshabilitarWifi();
                if (!disabled) {
                    Toast.makeText(OrientacionTest.this, "No se ha deshabilitado la conexión wifi",
                            Toast.LENGTH_LONG).show();
                }
            }
            if (mIsCheckedVibrate) {
                mUtils.deshabilitarVibracion();
            }
            //Silenciamos el dispositivo
            mUtils.silenciarDispositivo();
        }
    };
    /**
     * Click Listener para el botón configurar
     */
    View.OnClickListener configureClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            //Abrimos la pantalla de configuración de preferencias
            Intent intent = new Intent(OrientacionTest.this, PreferencesOrientacion.class);
            startActivity(intent);
        }
    };
}
