/**
 *
 */
package com.android.dgg.orientacion.utils;


/**
 * @author david
 *
 * Listener para los cambios en el acelerómetro
 */
public interface OrientacionListener  {
    /**
     * Acciones a ejecutar cuando ponemos el teléfono con la pantalla
     */
     void onScreenDown();
     /**
      * Acciones a ejecutar al poner el teléfono con la pantalla hacia arriba
      */
     void onScreenUp();
}
