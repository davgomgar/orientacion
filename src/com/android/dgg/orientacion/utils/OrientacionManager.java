/**
 *
 */
package com.android.dgg.orientacion.utils;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * Clase <code>OrientacionManager</code> Clase para gestionar el acelerómetro.
 * Esta clase está basada en el ejemplo
 * @author david
 */
public class OrientacionManager {
    /** Logger Tag*/
    private static final String TAG = "OrientacionManager";
    /** Context */
    private Context mContext = null;
    /** Sensor*/
    private  Sensor mSensor;
    /** SensorManager  */
    private  SensorManager mSensorManager = null;
    /** Listener para sensor de orientación */
    private  OrientacionListener mListener;
    /** Esta en ejecución sensor orientación? */
    private  boolean mRunning = false;


    /**
     * Método <code>isRunning</code> Indica si está el sensor orientación escuchando cambios de posición
     * @return True si está escuchando, false en caso contrario
     */
    public boolean isRunning() {
        return mRunning;
    }

    /**
     * @param mContext Contexto
     */
    public OrientacionManager(Context mContext) {
        this.mContext = mContext;
        initialize(mContext);
    }

    /**
     * Método <code>initialize</code> Método que inicializa sensor
     * @param mContext Contexto
     */
    private void initialize(Context mContext) {
         mSensorManager =
            (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensorsList = mSensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        if (!Collections.emptyList().equals(sensorsList)) {
            mSensor = sensorsList.get(0);
        }
    }



    /**
     * @param mContext Contexto
     * @param mThreshold Umbral
     * @param mInterval Intervalo
     * @param mListener Listener
     */
    public OrientacionManager(Context mContext,  OrientacionListener mListener) {
        this.mContext = mContext;
        this.mListener = mListener;
        initialize(this.mContext);
    }
    /**
     * Método <code>setOrientationListener</code> Setter para Listener del sensor de orientación
     * @param listener Listener
     */
    public void setOrientationListener(OrientacionListener listener) {
        this.mListener = listener;
    }
    /**
     * Método <code>getOrientationListener</code> Getter del Listener del sensor de orientación
     * @return Listener del acelerómetro
     */
    public OrientacionListener getOrientationListener() {
        return mListener;
    }
    /**
     * Método <code>startListening</code> Método que hace que se comience a escuchar los posibles
     * cambios del sensor de orientación
     */
    public void startListening() {
       mRunning =  mSensorManager.registerListener(mSensorEventListener, mSensor,
                SensorManager.SENSOR_DELAY_GAME);
    }
    /**
     * Método <code>stopListening</code> Método que hace que se deje de escuchar posibles cambios
     * del sensor de orientación
     */
    public void stopListening() {
        try {
            mSensorManager.unregisterListener(mSensorEventListener);
        } catch (Exception e) {
            Log.e(TAG, "Error al dejar de escuchar cambios acelerómetro.", e);
        }
    }
    /**
     * Método <code>isAccelerometerSupported</code> Indica si el dispositivo tiene acelerómetro
     * @return True si el dispositivo tiene acelerómetro / False en caso contrario
     */
    public Boolean isOrientationSensorSupported() {
        return (mSensor != null) ? Boolean.TRUE : Boolean.FALSE;
    }
    /**
     * SensorEventListener --> SensorEventListener. Internamente llamará a las operaciones
     * de OrientationListener.
     */
    private SensorEventListener mSensorEventListener = new SensorEventListener() {
        //Coordenadas en el espacio (X,Y,Z)
        private float mXcoord = 0f;
        private float mYcoord = 0f;
        private float mZcoord = 0f;
        //Error máximo aceptable
        private float EPSILON = 10f;
        // Valor que devuelve acelerómetro cuando movil está con pantalla hacia abajo
        private final float HUNDRED_EIGHTY_DEGREES = 180f;
        @Override
        public void onSensorChanged(SensorEvent event) {

            //Obtención de (X,Y,Z) para saber la posición del teléfono
            mXcoord = event.values[0];
            mYcoord = event.values[1];
            mZcoord = event.values[2];
            //Log de valores
            Log.d(TAG, "X Axis: " + mXcoord);
            Log.d(TAG, "Y Axis: " + mYcoord);
            Log.d(TAG, "Z Axis: " + mZcoord);
            //En función de rotacion mYcoord devuelve +-180 cuando está el móvil con la pantalla
            //hacia abajo
            if (Math.abs(mYcoord) + EPSILON > HUNDRED_EIGHTY_DEGREES) {
                mListener.onScreenDown();
            } else if (mYcoord - EPSILON <= 0f) {
                //El movil estaría con la pantalla hacia arriba
                mListener.onScreenUp();
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            Log.d(TAG, "onAccuracyChanged. " +
                    "Only here because of implementing SensorEventListener interface");
        }
    };



}
