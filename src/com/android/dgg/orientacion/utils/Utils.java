/**
 *
 */
package com.android.dgg.orientacion.utils;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Clase <code>Utils</code> Clase para ejecutar las tareas de habilitación / deshabilitación
 * @author david
 */
public class Utils {
    private static final String TAG = "util.Utils";
    private Context mContext;
    private ConnectivityManager mConnectivity;
    private NetworkInfo mNetInfoWifi;
    private WifiManager mWifiManager;
    private AudioManager mAudioManager;

    public Utils (Context context) {
        mContext = context;
        mConnectivity =
            (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        mNetInfoWifi = mConnectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
    }
    /**
     * Método <code>habilitarWifi</code> Método para habilitar una conexión wifi ya existente en
     * caso de que la red este disponible.
     * @return  True si se ha habilitado y conectado a la red / fallse en otro caso
     */
    public boolean habilitarWifi() {
        boolean connected = false;
        if(mNetInfoWifi != null && !mNetInfoWifi.isConnected()) {
            List<WifiConfiguration> configured = mWifiManager.getConfiguredNetworks();
            mWifiManager.startScan();
            List<ScanResult> scanned = mWifiManager.getScanResults();
            if (!configured.equals(Collections.EMPTY_LIST)
                    && !scanned.equals(Collections.EMPTY_LIST)) {
               for (ScanResult scan : scanned) {
                   for (WifiConfiguration config : configured) {
                       //Damos por hecho que si el BSSID es el mismo es la misma red
                       if (config.BSSID == scan.BSSID) {
                           //Habilitamos la wifi y nos conectamos a ella.
                           mWifiManager.setWifiEnabled(true);
                           connected = mWifiManager.enableNetwork(config.networkId, true);
                       }
                       break;
                   }
                   break;
               }
            }
        } else {
            Log.d(TAG, "No está conectada la red wifi");
        }
        return connected;
    }
    /**
     * Método <code>deshabilitarWifi</code> Método para deshabilitar la wifi de un dispositivo
     * @return True si se ha deshabilitado correctamente / false en otro caso
     */
    public boolean deshabilitarWifi() {
        if (mNetInfoWifi != null && mNetInfoWifi.isConnected()) {
            //Nos desconectamos de la red actual
            mWifiManager.disconnect();
            //Desconectamos la wifi
            final boolean disconnected = mWifiManager.setWifiEnabled(false);
            return disconnected;
        } else {
            Log.d(TAG, "La red wifi está desconectada");
        }
        return false;
    }
    /**
     * Método <code>silenciarDispositivo</code> Método para silenciar el dispositivo.
     */
    public void silenciarDispositivo() {
         mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
    }
    /**
     * Método <code>establecerTonoPorDefecto</code> Método para establecer el tono por defecto
     * del móvil.
     */
    public void establecerTonoPorDefecto() {
         mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
    }
    /**
     * Método <code>deshabilitarVibracion</code> Método para deshabilitar la vibración del teléfono
     */
    public boolean deshabilitarVibracion() {
        final boolean disabled = mAudioManager.shouldVibrate(AudioManager.VIBRATE_SETTING_OFF);
        return disabled;
    }
    /**
     * Método <code>habilitarVibracion</code> Método para habilitar la vibración del teléfono
     */
    public boolean habilitarVibracion(){
        final boolean enabled = mAudioManager.shouldVibrate(AudioManager.VIBRATE_SETTING_ON);
        return enabled;
    }

}
